# Infrastructure config for retailiate

## prepare dev project


- install freckles: https://freckles.io/download
- ``freckles config unlock``
- ``frecklecute --community -c allow_remote=true gl:retailiate::develop::/retailiate-infra/frecklets/retailiate-dev-project.frecklet --project-base <path_to_project_base>``

## k3s test cluster details:

- ip: 192.168.178.35
- connect: ssh markus@192.168.178.35

## Requirements

- kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/
- [argo](https://github.com/argoproj/argo/blob/master/demo.md): 
  - ``sudo curl -sSL -o /usr/local/bin/argo https://github.com/argoproj/argo/releases/download/v2.4.2/argo-linux-amd64``
  - ``sudo chmod +x /usr/local/bin/argo``
  
## Setup kubectl to access cluster

- ``mkdir ~/.kube/``
- ``scp markus@192.168.178.35:/etc/rancher/k3s/k3s.yaml ~/.kube/config``
- change server ip in ``~/.kube/config`` from https://127.0.0.1:6443 https://192.168.178.35:6443
- test: ``kubectl cluster-info``

## Connect to PostGRES

- execute: ``kubectl port-forward service/postgres 5432:5432``
- now Postgres is available on localhost, port 5432

## Connect to Metabase

- execute: `kubectl port-forward service/metabase 32624:32624`
- access: http://localhost:32624

## Connect to Kubernetes Dashboard

- ``kubectl proxy``
- for token, run: ``kubectl describe secrets dashboard-admin-sa-token-86x44``
- access: http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=default

## Connect to Argo dashboard:

- ``kubectl proxy``
- access: http://127.0.0.1:8001/api/v1/namespaces/argo/services/argo-ui/proxy/workflows

## submit Argo etl job

- ``cd k8s``
- ``argo submit etl_workflow.yml -w``

## display Argo details

- all jobs: ``argo list``
- job detail: ``argo get <job_name>``
- pod log (use pod name from above step): ``argo logs <pod_name>``
