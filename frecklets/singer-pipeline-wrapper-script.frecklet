args:
  path:
    doc:
      short_help: The path of the wrapper script.
    type: string
    required: true
  pipeline_path:
    doc:
      short_help: The base path of the pipeline directory.
    type: string
    required: true
  working_dir:
    doc:
      short_help: The path to the working dir, stores the state.json file.
    type: string
    required: true
    default: "${WORKING_DIR}"
  tap_name:
    doc:
      short_help: The name of the tap executable.
    type: string
    required: true
  target_name:
    doc:
      short_help: The name of the target executable.
    type: string
    required: true

frecklets:
  - shell-script-exists:
      path: "{{:: path ::}}"
      script_content: |
        #!/usr/bin/env bash

        WORKING_DIR="{{:: working_dir | default('') ::}}"

        TIMESTAMP=$(date "+%Y.%m.%d-%H.%M.%S")
        BACKUP_DIR="/home/freckles/backups"

        # set default working dir
        if [ -z "${WORKING_DIR}" ]
        then
            WORKING_DIR="/tmp/singer_pipeline"
        fi

        cd "${WORKING_DIR}"

        mkdir -p "${WORKING_DIR}/state"
        mkdir -p "${BACKUP_DIR}"
        mkdir -p "${WORKING_DIR}/config"

        state_arg=""
        state_path="${WORKING_DIR}/state/state.json"
        if [[ -f "${WORKING_DIR}/state/state.json" ]]; then
            if [[ -s "${WORKING_DIR}/state/state.json" ]]; then
                state_arg="--state ${WORKING_DIR}/state/state.json"
                # make backup
                cp ${WORKING_DIR}/state/state.json "${BACKUP_DIR}/state-${TIMESTAMP}.json"
            else
                # delete empty state file
                rm "${WORKING_DIR}/state/state.json"
            fi
        fi

        tap_arg=""
        if [[ -f "${WORKING_DIR}/config/tap_config.json" ]]; then
            if [[ -s "${WORKING_DIR}/config/tap_config.json" ]]; then
                tap_arg="--config ${WORKING_DIR}/config/tap_config.json"
            else
                # delete empty config file
                rm "${WORKING_DIR}/config/tap_config.json"
            fi
        fi

        target_arg=""
        if [[ -f "${WORKING_DIR}/config/target_config.json" ]]; then
            if [[ -s "${WORKING_DIR}/config/target_config.json" ]]; then
                target_arg="--config ${WORKING_DIR}/config/target_config.json"
            else
                # delete empty config file
                rm "${WORKING_DIR}/config/target_config.json"
            fi
        fi

        catalog_arg=""
        if [[ -f "${WORKING_DIR}/config/catalog.json" ]]; then
            if [[ -s "${WORKING_DIR}/config/catalog.json" ]]; then
                catalog_arg="--catalog ${WORKING_DIR}/config/catalog.json"
            fi
        fi

        echo "State dir:"
        ls -lah "${WORKING_DIR}/state"
        echo "Config dir:"
        ls -lah "${WORKING_DIR}/config"

        echo "tap"
        echo "${tap_arg}"
        cat "${WORKING_DIR}/config/tap_config.json"
        echo "target"
        echo "${target_arg}"
        cat "${WORKING_DIR}/config/target_config.json"
        echo "catalog"
        echo "${catalog_arg}"
        echo "state"
        echo "${state_arg}"
        echo "${state_path}"

        # run the pipeline
        {{:: pipeline_path ::}}/{{:: tap_name ::}} ${catalog_arg} ${tap_arg} ${state_arg} | {{:: pipeline_path ::}}/{{:: target_name ::}} ${target_arg} >> ${state_path}

        # clean up state.json file
        if [[ -f "${state_path}" ]]; then
            cp "${state_path}" "${BACKUP_DIR}/state-finished-${TIMESTAMP}.json"
            tail -1 "${state_path}" > "${BACKUP_DIR}/state-finished-cleaned-${TIMESTAMP}.json" && cp "${BACKUP_DIR}/state-finished-cleaned-${TIMESTAMP}.json" "${state_path}"
        fi
